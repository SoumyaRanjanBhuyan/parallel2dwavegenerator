This program implements a parallel wave generator using OpenMP and OpenMPI parallel programming. 
1. Uses a 2048 X 2048 grid. 
2. Generates waves for 2000 time steps. 
3. Finds the maximum amplitude across the entire domain. When this maximum is found to be 20% of the height of the original amplitude, launches another pulse from the boundary edge 90 degree clockwise. 
4. Refer the "SoumyaBhuyan_HPC_Project_2_Report.pdf" for detailed explanation and the speedup result.

![wave_1](https://bitbucket.org/SoumyaRanjanBhuyan/parallel2dwavegenerator/raw/ded87f15840ebfed5f4d87e413b0b2cdd0f47113/Screenshots/wave_1.png)
![wace_2](https://bitbucket.org/SoumyaRanjanBhuyan/parallel2dwavegenerator/raw/ded87f15840ebfed5f4d87e413b0b2cdd0f47113/Screenshots/wave_2.png)
![wave_3](https://bitbucket.org/SoumyaRanjanBhuyan/parallel2dwavegenerator/raw/ded87f15840ebfed5f4d87e413b0b2cdd0f47113/Screenshots/wave_3.png)
![wave_4](https://bitbucket.org/SoumyaRanjanBhuyan/parallel2dwavegenerator/raw/ded87f15840ebfed5f4d87e413b0b2cdd0f47113/Screenshots/wave_4.png)
![wave_5](https://bitbucket.org/SoumyaRanjanBhuyan/parallel2dwavegenerator/raw/ded87f15840ebfed5f4d87e413b0b2cdd0f47113/Screenshots/wave_5.png)