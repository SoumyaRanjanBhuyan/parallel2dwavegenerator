/********************************************/
/*  Author: Soumya R Bhuyan                 */
/*  CSE 598 Introduction to HPC             */
/*  Parallel 2D wave Solver                 */
/********************************************/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<mpi.h>

#define GRIDX 2048
#define GRIDY 2048
#define TIMESTEP 2000
#define PERTURB_POSITION 1024
#define INITIAL_AMPLITUDE 30.0
#define OPENGL_DATA_FILE "waveopengl240_200.txt"
#define FINALSTEP_FILE  "wavefinalstephybrid.txt"

double **alloc2d(double **arr2d, int ni, int nj);

int main(int argc, char *argv[]){
	double dx = (double)1/GRIDX;
	double dy = (double)1/GRIDY;
	double dt = (double)dx * 0.48;
	double r = dt/dx;
	double rsquare = r * r;
	int i,j,l,m,n;
	int local_rows;	
	int partition_size,each_step_partition,half_ghost_cell;
    FILE *wave_opengl_file, *wave_finalstep_file;
    char opengl_out[65], finalstep_out[100];
	
	double local_max_amplitude,global_max_amplitude;
	double attenuated_amplitude;
	int clear_boundary=0;

	int new_wave_info[2] = {0,0};
	int top = 1;
	double t1,t2;
	double **temp;
	
	double **u,**um,**up;
	
	//openMP variables
	int tid,numthreads,chunk,tc;
	double thread_max_arr[10];

	//MPI variables
	int rank, numprocs;
	MPI_Status status,status1,status2,status3,status4;

	//MPI Initialization
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

	//Allocate matrices
	u = alloc2d(u,GRIDX+2,GRIDY+2);
    um = alloc2d(um,GRIDX+2,GRIDY+2);
    up = alloc2d(up,GRIDX+2,GRIDY+2);
	
	//Attenuated Amplitude
	attenuated_amplitude = INITIAL_AMPLITUDE * 0.2;

    if(rank == 0){
        printf("Number of Processes = %d\n",numprocs);
    }

    if(GRIDX % numprocs != 0){
        if(rank == 0){
            printf("For this program to work correctly the GRIDX value should be an integral multiple of number of processes.\n");
            printf("GRIDX = %d, Number of Processes = %d ... Aborting! :-(\n",GRIDX,numprocs);
        }
		MPI_Finalize();
		exit(0);
    }

	/*************** Uncomment to write to opengl wave data file *************/
	/*************** Important: Un-comment the code that writes data each time step  ******/
/*
	//opengl file for writing wave data
	if(rank == 0){
		wave_opengl_file = fopen(OPENGL_DATA_FILE,"a+");
		sprintf(opengl_out,"%d|%d|%d",GRIDX,GRIDY,TIMESTEP);
        fprintf(wave_opengl_file,"%s",opengl_out);
	}
*/

	//1D Data Decomposition: partition size computation
	local_rows = GRIDX/numprocs;
	partition_size = (local_rows + 2) * (GRIDY + 2);
	each_step_partition = local_rows * (GRIDY+2);
	half_ghost_cell = (GRIDY+2)/2;
	
	//initial conditions: u matrices set to ZERO for l=0 and l=1  
	for(i=0;i < GRIDX+2;i++){
		for(j=0;j < GRIDY+2;j++){
			u[i][j]  = 0.0;
			um[i][j] = 0.0;
			up[i][j] = 0.0;
		}
	}

	t1 = MPI_Wtime();

	if(numprocs != 1){
		//Data Distribution: Process 0 sends data to each process (Allocated Partition + Ghost Cells)
		if(rank == 0){
			for(i=1;i < numprocs;i++){
				MPI_Send(&u[i*local_rows][0],partition_size,MPI_DOUBLE,i,1,MPI_COMM_WORLD);
			}
		}else{
			MPI_Recv(&u[0][0],partition_size,MPI_DOUBLE,0,1,MPI_COMM_WORLD,&status);
		}
	}

	t2 = MPI_Wtime();
	if(rank == 0){
		printf("Time Taken for Data Distribution = %f\n",t2-t1);
	}

    //Perturbing the system: Setting initial amplitude at the left boundary center
	if(rank == 0){
		u[0][PERTURB_POSITION] = INITIAL_AMPLITUDE;
		clear_boundary = 1;
	}

	#pragma omp parallel
    {
        numthreads = omp_get_num_threads();
        chunk = local_rows/numthreads;
		//printf("numthreads = %d\n",numthreads);
    }

    //Computation of wave using Finite-Difference Method
	for(l=1;l < TIMESTEP-1;l++){
        local_max_amplitude=0.0;
    #pragma omp parallel shared(u,um,up,rsquare,chunk,thread_max_arr) private(i,j,tc,tid,local_max_amplitude)
    {
        tid = omp_get_thread_num();

        #pragma omp for schedule(static,chunk)
		for(i=1;i < local_rows+1; i++){
			for(j=1;j < GRIDY+1;j++){
            	up[i][j] = (2*u[i][j]) - um[i][j] +
                           rsquare * (u[i+1][j] + u[i-1][j] + u[i][j+1] + u[i][j-1] - 4*u[i][j]);
 
            	if(up[i][j] > local_max_amplitude)
			   		local_max_amplitude = up[i][j];
			}
		}
		
        thread_max_arr[tid] = local_max_amplitude;
	}//end of parallel

    local_max_amplitude = thread_max_arr[0];
        for(tc=1;tc < numthreads;tc++){
            if(thread_max_arr[tc] > local_max_amplitude)
            local_max_amplitude = thread_max_arr[tc];
    }


      //Book-keeping for the next Timestep: Update ghost cells + send computed grid to rank 0 + find maximum amplitude
		//clear boundary
		if(numprocs == 1){
			if(clear_boundary == 1){
				//printf("Clearing boundary at l = %d\n",l);
				clear_boundary = 0;
				up[0][GRIDY/2] = 0.0;
				up[(local_rows/2)+1][GRIDY+1] = 0.0;
                up[local_rows+1][GRIDY/2] = 0.0;
                up[(local_rows/2)+1][0] = 0.0;
			}
		}else{
			if(clear_boundary == 1){
				if(rank == 0){
					clear_boundary = 0;
					up[0][GRIDY/2] = 0.0;
				}else if(rank == numprocs/2){
					if(top == 1){
						clear_boundary = 0;
						up[(local_rows/2)+1][0] = 0.0;
					}else{
                    	clear_boundary = 0;
                    	up[(local_rows/2)+1][GRIDY+1] = 0.0;
					}
				}else if(rank == numprocs-1){
					clear_boundary = 0;
					up[local_rows+1][GRIDY/2] = 0.0;
				}
			}
		}
/*
		//Find maximum local amplitude
		local_max_amplitude = up[1][1];
   		for(i=1;i < local_rows+1;i++){
			for(j=1;j < GRIDY+1;j++){
				if(up[i][j] > local_max_amplitude){
					local_max_amplitude = up[i][j];
				}
			}
		}
*/

		if(numprocs != 1){		
			//Find global maximum amplitude
			MPI_Reduce(&local_max_amplitude,&global_max_amplitude,1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
		}else{
			global_max_amplitude = local_max_amplitude;
		}	

		//if(rank == 0)
			//printf("global_max_amplitude = %f\n",global_max_amplitude);	
	
		//rank ZERO checks wheather to generate a wave
		if(rank == 0){
			if(global_max_amplitude < attenuated_amplitude){
				new_wave_info[0] = 1;
				if(new_wave_info[1] == 0 || new_wave_info[1] == 1 || new_wave_info[1] == 2)
					new_wave_info[1] += 1;
				else
					new_wave_info[1] = 0;
			}
		}
		
		//rank 0 Broadcasts new_wave_info
		if(numprocs != 1){
			MPI_Bcast(new_wave_info,2,MPI_INT,0,MPI_COMM_WORLD);	
		}

		if(numprocs==1){
			if(new_wave_info[0] == 1){
                if(new_wave_info[1] == 0){
                    clear_boundary = 1;
                    up[0][GRIDY/2] = INITIAL_AMPLITUDE;
                }else if(new_wave_info[1] == 1){
                    clear_boundary = 1;
                    up[(local_rows/2)+1][GRIDY+1] = INITIAL_AMPLITUDE;
                    //top = 0;
				}else if(new_wave_info[1] == 2){
                    clear_boundary = 1;
                    up[local_rows+1][GRIDY/2] = INITIAL_AMPLITUDE;
				}else if(new_wave_info[1] == 3){
                    clear_boundary = 1;
                    up[(local_rows/2)+1][0] = INITIAL_AMPLITUDE;
                    //top = 1;
				}
			}
		}else{
			if(new_wave_info[0] == 1){
            	if(rank == 0){
                	if(new_wave_info[1] == 0){
                    	clear_boundary = 1;
                    	up[0][GRIDY/2] = INITIAL_AMPLITUDE;
                	}
            	}else if(rank == numprocs/2){
                	if(new_wave_info[1] == 1){
                    	clear_boundary = 1;
                    	up[(local_rows/2)+1][GRIDY+1] = INITIAL_AMPLITUDE;
                    	top = 0;
                	}else if(new_wave_info[1] == 3){
                    	clear_boundary = 1;
                   	 	up[(local_rows/2)+1][0] = INITIAL_AMPLITUDE;
                    	top = 1;
               	 	}
            	}else if(rank == numprocs-1){
                	if(new_wave_info[1] == 2){
                    	clear_boundary = 1;
                    	up[local_rows+1][GRIDY/2] = INITIAL_AMPLITUDE;
               	 	}
            	}
        	}
		}

/*
		//Send computaed grid to rank 0 only at the last time step 
		if(l == TIMESTEP - 2){
			if(rank==0){
				for(i=1;i < numprocs;i++){
					MPI_Recv(&up[(i*local_rows)+1][0],each_step_partition,MPI_DOUBLE,i,1,MPI_COMM_WORLD,&status);
				}
			}else{
				MPI_Send(&up[1][0],each_step_partition,MPI_DOUBLE,0,1,MPI_COMM_WORLD);
			}
		}
*/		
	/******Rank ZERO collects data each step: Un-comment to write each step data to opengl file *********/
/*	
        if(rank==0){
        	for(i=1;i < numprocs;i++){
            	MPI_Recv(&up[(i*local_rows)+1][0],each_step_partition,MPI_DOUBLE,i,1,MPI_COMM_WORLD,&status);
            }
        }else{
        	MPI_Send(&up[1][0],each_step_partition,MPI_DOUBLE,0,1,MPI_COMM_WORLD);
        }
*/

	//Ghost Cell Update
	if(numprocs != 1){
		if(rank == 0){
			MPI_Recv(&up[local_rows+1][0],half_ghost_cell,MPI_DOUBLE,rank+1,1,MPI_COMM_WORLD,&status);
			MPI_Recv(&up[local_rows+1][half_ghost_cell],half_ghost_cell,MPI_DOUBLE,rank+1,2,MPI_COMM_WORLD,&status);
			MPI_Send(&up[local_rows][0],half_ghost_cell,MPI_DOUBLE,rank+1,3,MPI_COMM_WORLD);
			MPI_Send(&up[local_rows][half_ghost_cell],half_ghost_cell,MPI_DOUBLE,rank+1,4,MPI_COMM_WORLD);
		}else if(rank == (numprocs-1)){
			MPI_Send(&up[1][0],half_ghost_cell,MPI_DOUBLE,rank-1,1,MPI_COMM_WORLD);
			MPI_Send(&up[1][half_ghost_cell],half_ghost_cell,MPI_DOUBLE,rank-1,2,MPI_COMM_WORLD);
			MPI_Recv(&up[0][0],half_ghost_cell,MPI_DOUBLE,rank-1,3,MPI_COMM_WORLD,&status);
			MPI_Recv(&up[0][half_ghost_cell],half_ghost_cell,MPI_DOUBLE,rank-1,4,MPI_COMM_WORLD,&status);
		}else{
			MPI_Send(&up[1][0],half_ghost_cell,MPI_DOUBLE,rank-1,1,MPI_COMM_WORLD);
			MPI_Send(&up[1][half_ghost_cell],half_ghost_cell,MPI_DOUBLE,rank-1,2,MPI_COMM_WORLD);
			MPI_Send(&up[local_rows][0],half_ghost_cell,MPI_DOUBLE,rank+1,3,MPI_COMM_WORLD);
			MPI_Send(&up[local_rows][half_ghost_cell],half_ghost_cell,MPI_DOUBLE,rank+1,4,MPI_COMM_WORLD);
			MPI_Recv(&up[local_rows+1][0],half_ghost_cell,MPI_DOUBLE,rank+1,1,MPI_COMM_WORLD,&status);
			MPI_Recv(&up[local_rows+1][half_ghost_cell],half_ghost_cell,MPI_DOUBLE,rank+1,2,MPI_COMM_WORLD,&status);
			MPI_Recv(&up[0][0],half_ghost_cell,MPI_DOUBLE,rank-1,3,MPI_COMM_WORLD,&status);
			MPI_Recv(&up[0][half_ghost_cell],half_ghost_cell,MPI_DOUBLE,rank-1,4,MPI_COMM_WORLD,&status);
		}
	}

		//new Wave info have been exchanged by ghost cell updates: rank ZERO changes the new_wave_info to ZERO
		if(rank == 0)
			new_wave_info[0] = 0;

		if(numprocs != 1){
			MPI_Bcast(new_wave_info,1,MPI_INT,0,MPI_COMM_WORLD);
		}

		//um->u, u->up, up->um
		temp = um;
		um = u;
		u = up;
		up = temp;

		/*************** Uncomment to write to opengl wave data file *************/
	
/*		//writing wave data file to be processed by openGL
    	if(rank==0){
			for(i=1;i<GRIDX+1;i++){
				for(j=1;j<GRIDY+1;j++){
		        	sprintf(opengl_out,"\n%f",u[i][j]);
    		        fprintf(wave_opengl_file,"%s",opengl_out);
				}
			}
		}
*/		


	}//End of For

	t2 = MPI_Wtime();
	if(rank == 0){
		printf("MPI_TIME = %f\n",t2-t1);	
	}

/*************** Uncomment to write to opengl wave data file *************/
/*
	if(rank == 0){
    	sprintf(opengl_out,"\nENDOFFILE");
        fprintf(wave_opengl_file,"%s",opengl_out);
        fclose(wave_opengl_file);
	}
*/
/*
    //writing the wave data for the final timestep
    if(rank==0){
        wave_finalstep_file = fopen(FINALSTEP_FILE,"a+");
        fprintf(wave_finalstep_file,"%s","x\t y\t u[x][y]\n");
        for(i=1;i<GRIDX+1;i++){
            for(j=1;j<GRIDY+1;j++){
                sprintf(finalstep_out,"%d\t%d\t%f\n",i,j,u[i][j]);
                fprintf(wave_finalstep_file,"%s",finalstep_out);
            }
        }
        fclose(wave_finalstep_file);
    }   
*/

	//MPI Finalize
	MPI_Finalize();

	return 0;
}

double **alloc2d(double **arr2d, int ni, int nj) {
	int i,j;
	arr2d = (double **)malloc(ni*sizeof(double **));
	for(i=0; i < ni; i++){
 		arr2d[i]=(double *)malloc(nj*sizeof(double *));
	}
	return(arr2d);
}
